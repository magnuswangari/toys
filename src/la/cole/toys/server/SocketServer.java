package la.cole.toys.server;

import la.cole.toys.models.UIInteractor;
import la.cole.toys.utils.Config;
import la.cole.toys.utils.FileLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Manages network communication between the server and the client.
 */
public class SocketServer {
    private final UIInteractor uiInteractor;

    SocketServer(UIInteractor uiInteractor) {
        this.uiInteractor = uiInteractor;
    }

    void startListening() {
        FileLogger.resetFile();
        FileLogger.log("[Server] starting on port " + Config.port);
        try (
                ServerSocket serverSocket = new ServerSocket(3000);
                Socket clientSocket = serverSocket.accept();
                PrintWriter socketOut = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader socketIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))
        ) {
            String fromClient, output;

            ServerProtocol protocol = new ServerProtocol();

            while ((fromClient = socketIn.readLine()) != null) {
                FileLogger.log("[Server] from client -> " + fromClient);
                uiInteractor.log("[Server] from server -> " + fromClient);

                output = protocol.processInput(fromClient);
                if (output == null) {
                    continue;
                } else if (output.equals("EXIT")) {
                    break;
                }

                FileLogger.log("[Server] to client -> " + output);
                uiInteractor.log("[Server] to client -> " + output);

                socketOut.println(output);
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port " + Config.port + " or listening for a connection");
            System.out.println(e.getMessage());
            uiInteractor.log("Exception caught when trying to listen on port " + Config.port + " or listening for a connection");
            uiInteractor.log(e.getMessage());
        }
    }
}
