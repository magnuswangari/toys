package la.cole.toys.server;

import la.cole.toys.models.ThankYouMessage;
import la.cole.toys.utils.Actions;
import la.cole.toys.models.ToyDetails;
import la.cole.toys.models.ToyInformation;
import la.cole.toys.models.ToyManufacturer;

/**
 * Manages business logic interaction in response to the client.
 */
class ServerProtocol {

    String processInput(String input) {
        int action = Actions.getActionFromPack(input);
        String output;
        String strippedInput = Actions.strip(input);
        switch (action) {
            case Actions.CONNECT:
                output = handleConnect();
                break;
            case Actions.OK:
                output = handleOk();
                break;
            case Actions.SEND_TOY_DETAILS:
                ToyDetails toyDetails = ToyDetails.deserialize(strippedInput);
                output = handleSendToyDetails(toyDetails);
                break;
            case Actions.SEND_TOY_INFO:
                ToyInformation toyInformation = ToyInformation.deserialize(strippedInput);
                output = handleSendToyInfo(toyInformation);
                break;
            case Actions.SEND_TOY_MANUFACTURER:
                ToyManufacturer manufacturer = ToyManufacturer.deserialize(strippedInput);
                output = handleSendToyManufacturer(manufacturer);
                break;
            case Actions.SEND_THANKS:
                ThankYouMessage message = ThankYouMessage.deserialize(strippedInput);
                output = handleSendThanks(message);
                break;
            case Actions.SEND_ALL:
                String[] split = strippedInput.split(",");
                output = handleSendAll(ToyDetails.deserialize(split[0]), ToyInformation.deserialize(split[1]), ToyManufacturer.deserialize(split[2]), ThankYouMessage.deserialize(split[3]));
                break;
            case Actions.UNKNOWN:
                // Do nothing
            default:
                output = "EXIT";
        }
        return output;
    }

    private String handleOk() {
        return null;
    }

    private String handleConnect() {
        System.out.println("New client connected!");
        return Actions.pack(Actions.SEND_TOY_DETAILS, "");
    }

    private String handleSendToyDetails(ToyDetails toyDetails) {
        System.out.println("Received toy details");
        System.out.println(toyDetails.toString());
        return Actions.pack(Actions.SEND_TOY_INFO, "");
    }

    private String handleSendToyInfo(ToyInformation toyInformation) {
        System.out.println("Received toy info");
        System.out.println(toyInformation.toString());
        return Actions.pack(Actions.SEND_TOY_MANUFACTURER, "");
    }

    private String handleSendToyManufacturer(ToyManufacturer manufacturer) {
        System.out.println("Received toy manufacturer");
        System.out.println(manufacturer.toString());
        return Actions.pack(Actions.SEND_THANKS, "");
    }

    private String handleSendThanks(ThankYouMessage thankYouMessage) {
        System.out.println("Received thanks");
        System.out.println(thankYouMessage.toString());
        return Actions.pack(Actions.SEND_ALL, "");
    }

    private String handleSendAll(ToyDetails toyDetails, ToyInformation toyInformation, ToyManufacturer toyManufacturer, ThankYouMessage thankYouMessage) {
        System.out.println("Received send all");
        System.out.println(toyDetails);
        System.out.println(toyInformation);
        System.out.println(toyManufacturer);
        System.out.println(thankYouMessage);
        return Actions.pack(Actions.OK, "");
    }
}
