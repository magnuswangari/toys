package la.cole.toys.models;

/**
 * Thanks you message data model
 */
public class ThankYouMessage {
    private final String code;
    private final String message;

    public ThankYouMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String toString() {
        return "ThankYouMessage{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public static String serialize(ThankYouMessage input) {
        return String.format("%s;%s", input.code, input.message);
    }

    public static ThankYouMessage deserialize(String input) {
        String[] splits = input.split(";");
        return new ThankYouMessage(splits[0], splits[1]);
    }
}
