package la.cole.toys.models;

/**
 * Toy manufacturer data model
 */
public class ToyManufacturer {
    private String name;
    private String streetAddress;
    private String zipcode;
    private String country;

    public ToyManufacturer(String name, String streetAddress, String zipcode, String country) {
        this.name = name;
        this.streetAddress = streetAddress;
        this.zipcode = zipcode;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "ToyManufacturer{" +
                "name='" + name + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public static String serialize(ToyManufacturer input) {
        return String.format("%s;%s;%s;%s", input.name, input.streetAddress, input.zipcode, input.country);
    }

    public static ToyManufacturer deserialize(String input) {
        String[] splits = input.split(";");

        return new ToyManufacturer(splits[0], splits[1], splits[2], splits[3]);
    }
}
