package la.cole.toys.client;

import la.cole.toys.models.UIInteractor;
import la.cole.toys.utils.Actions;
import la.cole.toys.utils.Config;
import la.cole.toys.utils.FileLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Manages network communication between the client and the server.
 */
class SocketClient {
    private final UIInteractor uiInteractor;
    ClientProtocol protocol;

    SocketClient(UIInteractor uiInteractor) {
        this.uiInteractor = uiInteractor;
    }

    void startListening() {
        try (
                Socket kkSocket = new Socket(Config.hostname, 3000);
                PrintWriter socketOut = new PrintWriter(kkSocket.getOutputStream(), true);
                BufferedReader socketIn = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()))
        ) {
            String fromServer;

            // Create protocol instance
            protocol = new ClientProtocol(socketOut, uiInteractor);
            socketOut.println(Actions.pack(Actions.CONNECT, ""));

            FileLogger.log("[Client] Connected to server");
            uiInteractor.log("[Client] Connected to server");

            // Keep listening for messages from the server
            while ((fromServer = socketIn.readLine()) != null) {
                FileLogger.log("[Client] from server -> " + fromServer);
                uiInteractor.log("[Client] from server -> " + fromServer);
                protocol.processInput(fromServer);
            }
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + Config.hostname);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " + Config.hostname+ ":" + Config.port);
            uiInteractor.log("Couldn't get I/O for the connection to " + Config.hostname + ":" + Config.port);
        }
    }
}
