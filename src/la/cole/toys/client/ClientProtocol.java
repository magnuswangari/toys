package la.cole.toys.client;

import la.cole.toys.models.*;
import la.cole.toys.utils.Actions;
import la.cole.toys.utils.FileLogger;

import java.io.PrintWriter;

/**
 * Manages business logic interaction in reaction to the server
 */
class ClientProtocol {
    private final PrintWriter socketOut;
    private final UIInteractor uiInteractor;

    ClientProtocol(PrintWriter socketOut, UIInteractor uiInteractor) {
        this.socketOut = socketOut;
        this.uiInteractor = uiInteractor;
    }

    void processInput(String input) {
        int action = Actions.getActionFromPack(input);
        switch (action) {
            case Actions.OK:
                uiInteractor.log("Request succeeded");
                break;
            case Actions.SEND_TOY_DETAILS:
                uiInteractor.log("Toy details requested");
                break;
            case Actions.SEND_TOY_INFO:
                uiInteractor.log("Toy information requested");
                break;
            case Actions.SEND_TOY_MANUFACTURER:
                uiInteractor.log("Toy manufacturer requested");
                break;
            case Actions.SEND_THANKS:
                uiInteractor.log("Thank you message requested");
                break;
            case Actions.SEND_ALL:
                uiInteractor.log("All information requested");
                break;
            case Actions.UNKNOWN:
                // Do nothing
        }
    }

    void sendToyDetails(ToyDetails toyDetails) {
        String s = ToyDetails.serialize(toyDetails);
        String pack = Actions.pack(Actions.SEND_TOY_DETAILS, s);
        socketOut.println(pack);

        FileLogger.log("[Client] to server -> " + pack);
        uiInteractor.log("[Client] to server -> " + pack);
    }

    void sendToyInfo(ToyInformation toyInformation) {
        String s = ToyInformation.serialize(toyInformation);
        String pack = Actions.pack(Actions.SEND_TOY_INFO, s);
        socketOut.println(pack);

        FileLogger.log("[Client] to server -> " + pack);
        uiInteractor.log("[Client] to server -> " + pack);
    }

    void sendToyManufacturer(ToyManufacturer toyManufacturer) {
        String s = ToyManufacturer.serialize(toyManufacturer);
        String pack = Actions.pack(Actions.SEND_TOY_MANUFACTURER, s);
        socketOut.println(pack);

        FileLogger.log("[Client] to server -> " + pack);
        uiInteractor.log("[Client] to server -> " + pack);
    }

    void sendThanks(ThankYouMessage thankYouMessage) {
        String s = ThankYouMessage.serialize(thankYouMessage);
        String pack = Actions.pack(Actions.SEND_THANKS, s);
        socketOut.println(pack);

        FileLogger.log("[Client] to server -> " + pack);
        uiInteractor.log("[Client] to server -> " + pack);
    }

    void sendAll(ToyDetails toyDetails, ToyInformation toyInformation, ToyManufacturer toyManufacturer, ThankYouMessage thankYouMessage){
        String s1 = ToyDetails.serialize(toyDetails);
        String s2 = ToyInformation.serialize(toyInformation);
        String s3 = ToyManufacturer.serialize(toyManufacturer);
        String s4 = ThankYouMessage.serialize(thankYouMessage);

        String body = String.format("%s,%s,%s,%s", s1, s2, s3, s4);
        String pack = Actions.pack(Actions.SEND_ALL, body);
        socketOut.println(pack);

        FileLogger.log("[Client] to server -> " + pack);
        uiInteractor.log("[Client] to server -> " + pack);
    }
}
