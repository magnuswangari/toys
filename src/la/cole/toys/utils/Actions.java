package la.cole.toys.utils;

/**
 * Represents the actions that can be taken between the server and the client.
 * @see Actions#UNKNOWN
 * @see Actions#CONNECT
 * @see Actions#OK
 * @see Actions#SEND_TOY_DETAILS
 * @see Actions#SEND_TOY_INFO
 * @see Actions#SEND_TOY_MANUFACTURER
 * @see Actions#SEND_THANKS
 * @see Actions#SEND_ALL
 */
public class Actions {
    public static final int UNKNOWN = -1;
    public static final int CONNECT = 0;
    public static final int OK = 1;
    public static final int SEND_TOY_DETAILS = 2;
    public static final int SEND_TOY_INFO = 3;
    public static final int SEND_TOY_MANUFACTURER = 4;
    public static final int SEND_THANKS = 5;
    public static final int SEND_ALL = 6;

    private static String delimiter = "#";

    /**
     * Retrieves the action code from a packed string.
     * @param input A packed string in the form of a number followed by a hash(#) followed by other characters.
     * @see Actions#pack
     * @return The code for the action
     */
    public static int getActionFromPack(String input) {
        try {
            return Integer.parseInt(input.split(delimiter)[0]);
        } catch (NumberFormatException e) {
            System.out.println("Unknown action: " + input.split(delimiter)[0]);
            return UNKNOWN;
        }
    }

    /**
     * Removes the packing delimiter from the string
     * @param input A packed string in the form of a number followed by a hash(#) followed by other characters.
     * @see Actions#pack
     * @return The part of the string after the delimiter
     */
    public static String strip(String input) {
        try {
            return input.split(delimiter)[1];
        } catch (ArrayIndexOutOfBoundsException e) {
            return "";
        }
    }

    /**
     * Packs an action and a body in to a packed string.
     * @param action The action's code.
     * @param body The message body
     * @return A packed string
     */
    public static String pack(int action, String body) {
        return String.format("%d%s%s", action, delimiter, body);
    }

    /**
     * Get the string representation of an action.
     * @param action The action's code
     * @return A string
     */
    public static String pretty(int action) {
        switch (action) {
            case UNKNOWN:
                return "UNKNOWN";
            case CONNECT:
                return "CONNECT";
            case OK:
                return "OK";
            case SEND_TOY_DETAILS:
                return "SEND_TOY_DETAILS";
            case SEND_TOY_INFO:
                return "SEND_TOY_INFO";
            case SEND_TOY_MANUFACTURER:
                return "SEND_TOY_MANUFACTURER";
            case SEND_THANKS:
                return "SEND_THANKS";
            case SEND_ALL:
                return "SEND_ALL";
        }
        return "";
    }

}
