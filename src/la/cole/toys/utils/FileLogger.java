package la.cole.toys.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * File logger -> store logs to a file
 */
public class FileLogger {
    private static Path target = Paths.get("./log.txt");

    public static void resetFile() {
        try {
            Files.deleteIfExists(target);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void log(String line) {
        System.out.println(line);
        try {
            FileWriter fileWriter = new FileWriter(target.toString(), true);

            try (BufferedWriter writer = new BufferedWriter(fileWriter)) {
                writer.append(line, 0, line.length());
                writer.newLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
