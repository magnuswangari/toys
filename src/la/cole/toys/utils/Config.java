package la.cole.toys.utils;

/**
 * Application configuration
 */
public class Config {
    public static final int port = 3000;
    public static final String hostname = "0.0.0.0";
}
